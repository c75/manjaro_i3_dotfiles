#!/usr/bin/env bash

if pulseaudio --check; then
    echo Pulseaudio is running
else
    echo Pulseaudio is not running, starting it...
    pulseaudio -D
fi

# Set default sink
pactl set-default-sink alsa_output.pci-0000_00_1f.3.analog-stereo

# Unmute sink
pactl set-sink-mute alsa_output.pci-0000_00_1f.3.analog-stereo 0

# set normal volume
pactl set-sink-volume alsa_output.pci-0000_00_1f.3.analog-stereo 50%


# If this doesnt work, start pavucontrol and set output controls to Built in Audio Stereo.

