call plug#begin('~/.local/share/nvim/plugged')
    Plug 'vim-airline/vim-airline'
    Plug 'manasthakur/vim-commentor'
    Plug 'wvffle/vimterm'
    Plug 'mhinz/vim-grepper'
    Plug 'chrisbra/Colorizer'
    Plug 'vim-airline/vim-airline-themes'
		Plug 'ntpeters/vim-better-whitespace'
		Plug 'morhetz/gruvbox'
    Plug 'ncm2/ncm2' " nvim completion manager
    Plug 'roxma/nvim-yarp' " nvim completion manager
    Plug 'ncm2/ncm2-racer' " ncm2 completion source for rust
    Plug 'davidhalter/jedi-vim'
    Plug 'ctrlpvim/ctrlp.vim'
    Plug 'majutsushi/tagbar'
call plug#end()

syntax on
colorscheme gruvbox
set nohlsearch
set smartindent
set ignorecase
set smartcase
set autoindent expandtab tabstop=2 shiftwidth=2
set number relativenumber
set nowrap
set lazyredraw
set regexpengine=1
set foldcolumn=2
set clipboard=unnamedplus
set hidden
set undofile
let mapleader=","
nnoremap <silent> <leader>ev :edit $MYVIMRC<CR>
nnoremap gb :ls<CR>:b<Space>
nnoremap <silent> <leader>rv :so $MYVIMRC<CR>
nnoremap <silent> <Tab> :CtrlPMixed<CR>
nnoremap <silent> <A-h> :bprevious<CR>
nnoremap <silent> <A-l> :bnext<CR>
nnoremap <silent> <A-Left> :bprevious<CR>
nnoremap <silent> <A-Right> :bnext<CR>
nnoremap <silent> <F6> :call Update_Cloudformation_Stack()<CR>
nnoremap <silent> <leader><space> :nohlsearch<CR>
tnoremap <Esc> <C-\><C-n>
nnoremap <silent> <F8> :call vimterm#toggle() <CR>
tnoremap <F8> <C-\><C-n>:call vimterm#toggle() <CR>
tnoremap <silent> <C-Right> <C-\><C-n><c-w>l
tnoremap <silent> <C-Left> <C-\><C-n><c-w>h
tnoremap <silent> <C-Up> <C-\><C-n><c-w>k
tnoremap <silent> <C-Down> <C-\><C-n><c-w>j
nnoremap <silent> <C-Right> <C-\><C-n><c-w>l
nnoremap <silent> <C-Left> <C-\><C-n><c-w>h
nnoremap <silent> <C-Up> <C-\><C-n><c-w>k
nnoremap <silent> <C-Down> <C-\><C-n><c-w>j
nnoremap d "_d
nnoremap dd "_dd
nnoremap D "_D
nnoremap <Home> ^
nnoremap ^ <Home>
nnoremap <leader>* :Grepper -tool rg -cword -noprompt<cr>
nnoremap <leader>g :Grepper -tool rg <cr>

let g:airline_theme='tomorrow'
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#branch#enabled = 1
let g:better_whitespace_enabled=1
let g:airline#extensions#tabline#formatter = 'unique_tail'
let g:python3_host_prog = '/usr/bin/python3'

"let g:fzf_colors =
"\ { 'fg':      ['fg', 'Normal'],
"  \ 'bg':      ['bg', 'Normal'],
"  \ 'hl':      ['fg', 'Comment'],
"  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
"  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
"  \ 'hl+':     ['fg', 'Statement'],
"  \ 'info':    ['fg', 'PreProc'],
"  \ 'border':  ['fg', 'Ignore'],
"  \ 'prompt':  ['fg', 'Conditional'],
"  \ 'pointer': ['fg', 'Exception'],
"  \ 'marker':  ['fg', 'Keyword'],
"  \ 'spinner': ['fg', 'Label'],
"  \ 'header':  ['fg', 'Comment'] }

function! Update_Cloudformation_Stack()
    let currentFilePath = expand("%:p")
    let script = fnamemodify( currentFilePath, ':p:h') . "/upload_nested_stacks.sh"
    call vimterm#run(script)
endfunction


